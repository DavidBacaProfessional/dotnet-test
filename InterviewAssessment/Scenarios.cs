﻿using System.Collections.Generic;
using System;

namespace InterviewAssessment
{
    public class Scenarios
    {
        /// <summary>
        /// Implement this method.
        /// </summary>
        public string Scenario1(string str)
        {
            char[] input = str.ToCharArray();
            char[] output = new char[str.Length];

            //Reverse the string
            for (int i = 1; i < input.Length + 1; i++)
            {
                output[i - 1] = input[input.Length - i];
            }

            //Cast the char[] to a string
            return new string(output);

            /*Could convert to an array and use Array.Reverse function
              but that feels like cheating
              char[] output = str.ToCharArray();
              Array.Reverse(output);
              return new string(output);*/
        }

        /// <summary>
        /// Fix this method.
        /// </summary>
        public int Scenario2(int @base, int exponent)
        {
            int n = 1;

            //Off by 1 error corrected
            for (int i = 0; i < exponent; i++)
            {
                n *= @base;
            }

            return n;
        }

        /// <summary>
        /// DO NOT MODIFY
        /// </summary>
        public string Scenario3<T>(T obj) => typeof(T).Name;

        /// <summary>
        /// DO NOT MODIFY
        /// </summary>
        public string Scenario3(string str) => str;

        /// <summary>
        /// Implement this method.
        /// </summary>
        public string Scenario4(Node node)
        {
            if (node.GetType().Name == "Tree")
            {
                //Call a new function that can access the children of the tree
                return traverseTree((Tree)node);
            }
            else
            {
                return node.Text;
            }
        }

        /// <summary>
        /// Traverse through a tree object and return the text of each child node
        /// Note: The input is assumed to be the root node of the tree
        /// </summary>
        private string traverseTree(Tree tree)
        {
            string output = tree.Text;

            if (tree.Children != null)
            {
                foreach (Node n in tree.Children)
                {
                    //Recursively navigate through the tree and update the output
                    output += "-" + Scenario4(n);
                }
            }

            return output;
        }
    }

    public class Node
    {
        public Node(string text)
        {
            Text = text;
        }

        public string Text { get; set; }
    }

    public class Tree : Node
    {
        public Tree(string text, params Node[] children) : base(text)
        {
            Children = children;
        }

        public IEnumerable<Node> Children { get; set; }
    }
}
